# README #

Implementation of a Club Directory in JEE. You can show the entire class, add rows, delete rows and go to details. The interface will be accessible with a login/password.

### Fonctionnalités ajoutées ###

* Users sessions  that allows users and transaction information to persist between interactions.
* We introduce also a "logout" system with "session.invalidate()" that allows users to close sessions.

### Contribution ###

* Audouin d'Aboville
* Timothée Barbot
* Adrien Herbert

### Version ###
* V 1.0
* Codé en J2E - BDD Derby SQL

### Infos ###

EFREI : Projet JEE - M1 - 2016
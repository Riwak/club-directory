package m1.jee.ctrl;

public class Member {
    
        private int id;
        private String name;
    	private String surname;
        private String mobile;
        private String telephone;
        private String professionnal;
        private String adress;
        private String city;
        private String email;
        private String postalcode;

	public int getID() {
            return this.id;
	}
        
        public void setID( int value ) {
            this.id = value;
        }
        
        public String getName() {
            return this.name;
	}
        
        public void setName( String value ) {
            this.name = value;
        }
        
        public String getSurname() {
            return this.surname;
	}
        
        public void setSurname( String value ) {
            this.surname = value;
        }
        
        public String getMobile() {
            return this.mobile;
	}
        
        public void setMobile( String value ) {
            this.mobile = value;
        }
    
        public String getTelephone() {
            return this.telephone;
	}
        
        public void setTelephone( String value ) {
            this.telephone = value;
        }
        
        public String getProfessionnal() {
            return this.professionnal;
	}
        
        public void setProfessionnal( String value ) {
            this.professionnal = value;
        }
        
        public String getAdress() {
            return this.adress;
	}
        
        public void setAdress( String value ) {
            this.adress = value;
        }
        
        public String getCity() {
            return this.city;
	}
        
        public void setCity( String value ) {
            this.city = value;
        }
        
        public String getEmail() {
            return this.email;
	}
        
        public void setEmail( String value ) {
            this.email = value;
        }
        
        public String getPostalCode() {
            return this.postalcode;
	}
        
        public void setPostalCode( String value ) {
            this.postalcode = value;
        }
}

package m1.jee.ctrl;

public class Credentials {
    
        private String login;
	private String password;
        
        public Credentials( String value, String value2)
        {
            super();
            this.login = value;
            this.password = value2;
        }

	public String getLogin() {
            return this.login;
	}
        
        public String getPassword() {
           return this.password;     
        }
        
        public void setLogin( String value ) {
            this.login = value;
	}
        
        public void setPassword( String value ) {
            this.password = value;
        }
        
        public Credentials()
        {
            super();
        }
}

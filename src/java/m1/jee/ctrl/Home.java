package m1.jee.ctrl;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Home extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        
         // Gestion des messages
        PrintWriter out = response.getWriter();
                
        // Gestion des erreurs
        HashMap errMap = null;
      
        // Récupération de la session
        Credentials admin = new Credentials();
        HttpSession admin_Session = request.getSession();
        admin = (Credentials) admin_Session.getAttribute("User");
       
        if (admin!=null) // Vérification que le client est connecté
        {
            ArrayList <Member> listOfMember = new ArrayList<Member>();

            String req = "SELECT * FROM MEMBERS";
            Connection con = DBConnection.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(req);
        
            while(rs.next())
            {
                Member myMember = new Member();

                myMember.setID(Integer.parseInt(rs.getString("ID")));
                myMember.setSurname(rs.getString("NAME"));
                myMember.setName(rs.getString("FIRSTNAME"));
                myMember.setTelephone(rs.getString("TELHOME"));
                myMember.setMobile(rs.getString("TELMOB"));
                myMember.setProfessionnal(rs.getString("TELPRO"));
                myMember.setAdress(rs.getString("ADRESS"));
                myMember.setPostalCode(rs.getString("POSTALCODE"));
                myMember.setCity(rs.getString("CITY"));
                myMember.setEmail(rs.getString("EMAIL"));
                
                listOfMember.add(myMember);
            }
        
            request.setAttribute( "ListOfMembers", listOfMember);
            request.setAttribute( "admin", admin );
            DBdisconnect.logout(rs,stmt,con);
            this.getServletContext().getRequestDispatcher( "/WEB-INF/home.jsp" ).forward( request, response );
        }
        else
        {
            // redirection le client n'est pas connecté
            errMap.put("ERROR","Vous avez été déconnecté");
            request.setAttribute( "ERRORS", errMap);
            this.getServletContext().getRequestDispatcher( "/MyClass" ).forward( request, response );
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(Home.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(Home.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

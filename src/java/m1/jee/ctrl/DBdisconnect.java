package m1.jee.ctrl;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBdisconnect {
    
    public static void logout(ResultSet rs,Statement stmt,Connection conn) throws SQLException
    {
        rs.close();
        stmt.close();
        conn.close();
    }
    
    public static void logout2(int rs,Statement stmt,Connection conn) throws SQLException
    {
        rs=0;
        stmt.close();
        conn.close();
    }
}

package m1.jee.ctrl;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DBConnection {

    private static String url = "jdbc:derby://localhost:1527/Club-Directory";      
    private static String username = "root";   
    private static String password = "root";
    private static Connection con;

    public static Connection getConnection() {
            try {
                con = DriverManager.getConnection(url, username, password);
            } 
            catch (SQLException ex) 
            {
                out.println("Failed to create the database connection."); 
            }
        return con;
    }
}

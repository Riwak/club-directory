package m1.jee.ctrl;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Action extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        
        // Gestion des messages
        PrintWriter out = response.getWriter();
        
        // Gestion des erreurs
        HashMap errMap = null;
        
        // Récupération de la session
        Credentials admin = new Credentials();
        HttpSession admin_Session = request.getSession();
        admin = (Credentials) admin_Session.getAttribute("User");
       
        if (admin!=null) // Vérification que le client est connecté
        {
            // Utilisateur connecté
            // Récupération de la "demande"
            String id = request.getParameter( "id" );
            String action = request.getParameter( "action" );
            
            if ("Details".equals(action))
            {
                if (id!=null)
                {
                    String req = "SELECT * FROM MEMBERS WHERE ID=" + id;
                    Connection con = DBConnection.getConnection();
                    Statement stmt = con.createStatement();
                    ResultSet rs = stmt.executeQuery(req);
                
                    Member myMember = new Member();
        
                    while(rs.next())
                    {
                        myMember.setID(Integer.parseInt(rs.getString("ID")));
                        myMember.setSurname(rs.getString("NAME"));
                        myMember.setName(rs.getString("FIRSTNAME"));
                        myMember.setTelephone(rs.getString("TELHOME"));
                        myMember.setMobile(rs.getString("TELMOB"));
                        myMember.setProfessionnal(rs.getString("TELPRO"));
                        myMember.setAdress(rs.getString("ADRESS"));
                        myMember.setPostalCode(rs.getString("POSTALCODE"));
                        myMember.setCity(rs.getString("CITY"));
                        myMember.setEmail(rs.getString("EMAIL"));
                    }

                    request.setAttribute( "Member", myMember);
                    DBdisconnect.logout(rs,stmt,con);
                    this.getServletContext().getRequestDispatcher( "/WEB-INF/details.jsp" ).forward( request, response );
                }
                else
                {
                    errMap.put("ERROR","Aucune selection");
                    request.setAttribute( "ERRORS", errMap);
                    this.getServletContext().getRequestDispatcher( "/Home" ).forward( request, response );
                }
            }
            else if ("Delete".equals(action))
            {
                if (id!=null)
                {
                    String req = "DELETE FROM MEMBERS WHERE ID=" + id;
                    Connection con = DBConnection.getConnection();
                    Statement stmt = con.createStatement();
                    int rs = stmt.executeUpdate(req);
                    DBdisconnect.logout2(rs,stmt,con);
                    this.getServletContext().getRequestDispatcher( "/Home" ).forward( request, response );
                }
                else
                {
                    errMap.put("ERROR","Aucune selection");
                    request.setAttribute( "ERRORS", errMap);
                    this.getServletContext().getRequestDispatcher( "/Home" ).forward( request, response );
                }
            }
            else if ("Add".equals(action))
            {
                String req = "INSERT INTO MEMBERS(NAME,FIRSTNAME,TELHOME,TELMOB,TELPRO,ADRESS,POSTALCODE,CITY,EMAIL) VALUES\n" +
                "('Simpson','Homer','0123456789','0612345678','0698765432','2 avenue Duff','92700','Colombes','hsimpson@gmail.com'),\n" +
                "('Simpson','Bart','0145362787','0645362718','0611563477','10 rue des Rebelles','92270','Bois-colombes','bsimpson@gmail.com'),\n" +
                "('Lagaffe','Gaston','0187665987','0623334256','0654778654','65 rue de la Paresse','92700','Colombes','glagaffe@yahoo.fr'),\n" +
                "('Mafalda','Querida','0187611987','0783334256','0658878654','6 rue de Buenos Aires','75016','Paris','qmafalda@hotmail.ar'),\n" +
                "('Woodpecker','Woody','0187384987','0622494256','0674178654','5 bvd des Picoreurs','21000','Dijon','woody@mail.co.uk'),\n" +
                "('Brown','Charlie','0122456678','0699854673','0623445166','140 avenue Foche','90000','Nanterre','cbrown@live.com')";
                Connection con = DBConnection.getConnection();
                Statement stmt = con.createStatement();
                int rs = stmt.executeUpdate(req);
                DBdisconnect.logout2(rs,stmt,con);
                this.getServletContext().getRequestDispatcher( "/Home" ).forward( request, response );
            }
            else if ("Logout".equals(action))
            {
                admin=null;
                admin_Session.invalidate();
                errMap.put("ERROR","You have been disconnected");
                request.setAttribute( "ERRORS", errMap);
                this.getServletContext().getRequestDispatcher( "/MyClass" ).forward( request, response );
            }
            else
            {
                errMap.put("ERROR","Formulaire(Action) inconnu");
                request.setAttribute( "ERRORS", errMap);
                this.getServletContext().getRequestDispatcher( "/Home" ).forward( request, response );
            }
                
        }
        else
        {
            errMap.put("ERROR","Vous avez été déconnecté");
            request.setAttribute( "ERRORS", errMap);
            this.getServletContext().getRequestDispatcher( "/MyClass" ).forward( request, response );
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(Action.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(Action.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

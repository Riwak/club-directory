<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="m1.jee.ctrl.Member"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
    </head>
    <body>
        <h2>Welcome ${admin.login} </h2>
        <hr style="width:400px; float: left;">
        </br>
        <h2> List of members of the Java EE - M1 Club </h2> 
        
        
        <form action="Action" method="post" class="form-signin" role="form">
            
        <c:if test="${not empty ListOfMembers}">
        <table border="1">
            <tr>
                <th style="width: 20px">Select</th>
                <th style="width: 200px">First name</th>
                <th style="width: 200px">Last name</th>
                <th style="width: 200px">Email</th>
            </tr>
            <c:forEach items="${ListOfMembers}" var="Member">
                <tr>
                    <td style="width: 20px"><input type="radio" name="id" value="${Member.getID()}"/></td>
                    <td style="width: 200px"><c:out value="${Member.surname}" /></td>
                    <td style="width: 200px"><c:out value="${Member.name}"/></td>
                    <td style="width: 200px"><c:out value="${Member.email}"/></td>
                </tr>
            </c:forEach>
        </table>
        </c:if>
        
        <c:if test="${empty ListOfMembers}">
            <p style="color:blue; font-weight: bold;">The Club has no member!</p>
            <button type="submit" value="Add" name="action">Add</button>
        </c:if>
            
            </br>
            
            <button type="submit" value="Details" name="action">Details</button>
            <button type="submit" value="Delete" name="action">Delete</button>
            <button type="submit" value="Logout" name="action">Logout</button>
        </form>
        
        
    </body>
</html>

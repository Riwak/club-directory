<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Authentification page</title>
    </head>
    <body>
        <h3>EFREI - JAVA EE - M1</h3>
        </br>
        <p style="color:red;">${ERRORS}</p>
        <p>Enter your credentials : </p>
       
        <form action="Controller" method="post" class="form-signin" role="form"> <!-- Appeller la servlet-->
            <input type="text" placeholder="Pseudo" value="${login}" name="login"></br>
            <input type="password" name="pass" value="${password}" placeholder="Mot de passe"></br>
            <button type="submit" value="Connexion" name="Connexion">Se connecter</button>
        </form>
        <form action="MyClass" method="post" class="form-signin" role="form">
            <button type="submit" value="init" name="init">ContextAutoLogin</button>
        </form>
        
    </body>
</html>

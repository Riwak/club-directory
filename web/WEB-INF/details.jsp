<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="m1.jee.ctrl.Member"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Details</title>
    </head>
    <body>
       
        <h2> Member ${Member.name} </h2>
        Last name : ${Member.name} | First name : ${Member.surname}</br>
        <hr>
        <h3>Phone number</h3>
        Home number : ${Member.telephone}</br>
        Mobile number : ${Member.mobile}</br>
        Office number : ${Member.professionnal}</br>
        </br>
        Adress : ${Member.adress}</br>
        Postal code : ${Member.postalCode} | City : ${Member.city}</br>
        Email : ${Member.email}</br>
        <hr>
        <a href="Home"/> Back to menu </a>
        
    </body>
</html>
